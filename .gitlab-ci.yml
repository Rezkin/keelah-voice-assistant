# Define our main stages, in the order they should run.
# Each stage has one or more jobs that must all complete before the stage is considered complete.
stages:
  - environment
  - build
  - test

# Handles building and pushing a new Docker container to the registry.
# This is only run if we need to build an image.  If an updated image already exists, this won't
# run.
.buildContainer:
  # Use a baseline image that contains Docker itself.
  image: docker:stable
  stage: environment
  services:
    - docker:dind
  script:
    # Login to the docker registry local to repository.  The $CI_JOB_TOKEN and $CI_REGISTRY
    # variables are set by GitLab for us.
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # Attempt to pull the image already in the registry, but if it fails output true anyways.
    # We want to try and use it as a cache if possible, but if no image exists we will still build
    # one.
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG || true
    # Build the image in our repo using an existing image from the cache we just attempted to pull.
    # This assumes the name of our image is Dockerfile and is at the current directory (.)
    # We will tag it based on the commit of the repo.
    - docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    # Finally, push our new image to the registry.
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

# Runs the .buildContainer job only if the Dockerfile has been changed.
buildContainerUpdates:
  extends: .buildContainer
  only:
    changes:
      - Dockerfile

# Checks to make sure we have an image, and if not builds one.  Does not run if changes were made
# to the Dockerfile since we would run buildContainerUpdates instead.
ensureContainerExists:
  extends: .buildContainer
  # Since this job just makes sure we have an image, it's OK for this to fail.
  allow_failure: true
  # Since extending another stage means we run that stage first, we need a before_script to check
  # if we even need to run .buildContainer.
  before_script:
    - "mkdir -p ~/.docker && echo '{\"experimental\": \"enabled\"}' > ~/.docker/config.json"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # Skip update container `script` if the container already exists
    # via https://gitlab.com/gitlab-org/gitlab-ce/issues/26866#note_97609397 -> https://stackoverflow.com/a/52077071/796832
    - docker manifest inspect $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG > /dev/null && exit || true
  except:
    changes:
      - Dockerfile

# Generic set up steps prior to building or testing.
.setUp:
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    # Make sure we set any environment variables and our gradlew script has the appropriate
    # read/write permissions.
    - chmod +x gradlew
    - "export VERSION_CODE=$(($CI_PIPELINE_IID)) && echo $VERSION_CODE"
    - "export VERSION_SHA=`echo ${CI_COMMIT_SHA:0:8}` && echo $VERSION_SHA"
  # Set the build artifacts so we can store the results if we wish to publish later.
  artifacts:
    paths:
    - app/build/outputs

# Builds a debug version of the app.
buildDebug:
  # Perform generic set up.
  extends: .setUp
  stage: build
  script:
    - bundle exec fastlane buildDebug

# Add any other build steps here.

# Run any tests we have.
test:
  # Perform generic set up.
  extends: .setUp
  stage: test
  script:
    - bundle exec fastlane test
  # Output the test reports as artifacts to the build.
  artifacts:
    when: always
    reports:
      # Each type of testing framework outputs testing results differently.  If the testing
      # framework changes, then this should be changed as well.
      junit: build/test-results/test/**/TEST-*.xml
